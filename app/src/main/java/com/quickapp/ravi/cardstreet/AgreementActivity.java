package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.quickapp.ravi.dao.CountryDao;
import com.quickapp.ravi.model.Country;


public class AgreementActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner dropdown;
    String selectedCtry = "select";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);

        dropdown = (Spinner)findViewById(R.id.btnCountry);
        String[] items = new String[]{"Select Country","India", "Singapore"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(this);

        Button btnAccept = (Button) findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             Log.e("On accept", "On accept");
                                             if (!"select".equals(selectedCtry)) {
                                                 Country ctry = new Country(selectedCtry, "Accept");
                                                 CountryDao.open(AgreementActivity.this);
                                                 CountryDao.saveCountry(ctry);
                                                 CountryDao.close();
                                                 Intent intent = new Intent(AgreementActivity.this, AppLoadActivity.class);
                                                 AgreementActivity.this.startActivity(intent);
                                             }
                                         }
                                     }
        );

        Button btnReject = (Button) findViewById(R.id.btnReject);
        btnReject.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             finish();
                                             moveTaskToBack(true);
                                         }
                                     }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_agreement, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if(position == 1) {
            selectedCtry = "india";

        } else if( position == 2) {
            selectedCtry = "singapore";

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
