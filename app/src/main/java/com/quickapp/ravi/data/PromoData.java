package com.quickapp.ravi.data;

import com.quickapp.ravi.model.Promotion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/13/15.
 */
public class PromoData {

    public static List<Promotion> getPromoData() {
        List<Promotion> promotionList = new ArrayList<>();
        getPromoList(promotionList);
        return promotionList;
    }

    private static void getPromoList(List<Promotion> promoList) {

        Promotion p1 = new Promotion("8D Travel North Island in Style from S$16,888","Exclusively for DBS Altitude and DBS Black Cards"
                ,"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg",1);
        Promotion p2 = new Promotion("Get 50% more miles* with DBS Altitude American Express® Card","Apply now to enjoy 50% more miles on all your spend for 6 months"
                ,"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg",2);
        Promotion p3 = new Promotion("Free Adventure Cove Waterpark & Trick Eye Museum Bundle","Apply for DBS Live Fresh Student Card now and receive Adventure Cove Waterpark & Trick Eye Museum Bundle."
                ,"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg",3);
        Promotion p4 = new Promotion("Split your shopping bills into monthly instalments","Convert your Credit Card shopping bills into a 12-month 0% interest instalment plan now!"
                ,"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg",4);
        Promotion p5 = new Promotion("S$70 fuel voucher with DBS Esso MasterCard®Card","Apply now and receive S$70 fuel voucher."
                ,"http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg",5);

        promoList.add(p1);
        promoList.add(p2);
        promoList.add(p3);
        promoList.add(p4);
        promoList.add(p5);


    }

}
