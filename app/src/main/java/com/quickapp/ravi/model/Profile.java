package com.quickapp.ravi.model;

/**
 * Created by ravirane on 8/16/15.
 */
public class Profile {
    public String profileName;
    public String mailId;
    public String age;
    public String monthlyIncome;
    public String contact;

    public Profile() {

    }

    public Profile(String profileName, String mailId, String age, String monthlyIncome) {
        this.profileName = profileName;
        this.mailId = mailId;
        this.age = age;
        this.monthlyIncome = monthlyIncome;
    }
}
