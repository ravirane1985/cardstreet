package com.quickapp.ravi.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.quickapp.ravi.cardstreet.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/7/15.
 */
public class FiFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    FragmentManager fm;

    private int[] imageResId = {R.drawable.ic_action_bank,R.drawable.ic_action_card,R.drawable.ic_action_promo };


    private final List<Fragment> mFragments = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();

    public FiFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.fm = fm;
        this.context = context;
    }

    public void addFragment(Fragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }


    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        ImageView img = (ImageView) v.findViewById(R.id.tabImageView);
        img.setImageResource(imageResId[position]);
        return v;
    }

}
