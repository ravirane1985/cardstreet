package com.quickapp.ravi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickapp.ravi.cardstreet.R;

import java.util.List;

/**
 * Created by ravirane on 8/12/15.
 */
public class CardDetailsRecyclerAdapter  extends RecyclerView.Adapter<CardDetailsRecyclerAdapter.CardDetailsViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<String> cardDetails;

    public static class CardDetailsViewHolder extends RecyclerView.ViewHolder {

        public final TextView txtCardDetails;
        public final View mView;


        public CardDetailsViewHolder(View view) {
            super(view);
            mView = view;
            txtCardDetails = (TextView) view.findViewById(R.id.txtCardDetailRecord);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtCardDetails.getText();
        }
    }


    public String getValueAt(int position) {
        return cardDetails.get(position);
    }

    public CardDetailsRecyclerAdapter(Context context, List<String> cardDetails) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.cardDetails = cardDetails;
    }

    @Override
    public CardDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_details_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new CardDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CardDetailsViewHolder holder, int position) {
        String cardDetail = cardDetails.get(position);
        holder.txtCardDetails.setText(cardDetail);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Context context = v.getContext();
                Intent intent = new Intent(context, BankCardActivity.class);
                context.startActivity(intent);*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return cardDetails.size();
    }


}
