package com.quickapp.ravi.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.Profile;


/**
 * Created by ravirane on 8/16/15.
 */
public class ProfileDao extends BaseDao{

    public ProfileDao(Context context) {
        super(context);
    }

    public static Profile getProfile(Context context) {
        open(context);
        String selectQuery = "SELECT profileName,mailId,age,monthlyIncome,contact FROM PROFILE";
        Cursor mCursor = db.rawQuery(selectQuery, null);
        Profile profile = null;
        if (mCursor.moveToFirst()) {
            profile = new Profile();
            profile.profileName = mCursor.getString(mCursor.getColumnIndexOrThrow("profileName"));
            profile.contact = mCursor.getString(mCursor.getColumnIndexOrThrow("contact"));
            profile.monthlyIncome = mCursor.getString(mCursor.getColumnIndexOrThrow("monthlyIncome"));
            profile.age = mCursor.getString(mCursor.getColumnIndexOrThrow("age"));
            profile.mailId = mCursor.getString(mCursor.getColumnIndexOrThrow("mailId"));
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return profile;
    }


    public static void saveProfile(Context context,Profile profile) {
        open(context);
        ContentValues values = new ContentValues();
        clearProfile();
        values.put("profileName", profile.profileName);
        values.put("mailId",profile.mailId);
        values.put("age",profile.age);
        values.put("monthlyIncome",profile.monthlyIncome);
        values.put("contact",profile.contact);
        db.insert("PROFILE", null, values);
        close();
    }

    private static void clearProfile() {
         String deleteProfile = "delete from profile";
         db.execSQL(deleteProfile);
    }


}
