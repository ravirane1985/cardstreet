package com.quickapp.ravi.model;

/**
 * Created by ravirane on 8/13/15.
 */
public class Promotion {

    public String promoHeader;
    public String promoShortDetails;
    public String promoLogo;
    public int cardId;

    public Promotion(String promoHeader, String promoShortDetails, String promoLogo, int cardId) {
        this.promoHeader = promoHeader;
        this.promoShortDetails = promoShortDetails;
        this.promoLogo = promoLogo;
        this.cardId = cardId;
    }
}
