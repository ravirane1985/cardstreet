package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.adapter.CardRecyclerListAdapter;
import com.quickapp.ravi.dao.InterestDao;


public class InterestCardActivity extends AppCompatActivity {

    private RecyclerView interestCardView;
    private CardRecyclerListAdapter interestCardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_card);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize recycler view
        interestCardView = (RecyclerView) findViewById(R.id.recyclerviewInterestCard);
        interestCardView.setLayoutManager(new LinearLayoutManager(this));
        interestCardAdapter = new CardRecyclerListAdapter(InterestCardActivity.this, InterestDao.getInterestCards(getApplicationContext()), "interestcardactivity");
        interestCardView.setAdapter(interestCardAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_interest_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(InterestCardActivity.this, FIDashBoardActivity.class);
            intent.putExtra("tabNo",0);
            InterestCardActivity.this.startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
