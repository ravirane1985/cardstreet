package com.quickapp.ravi.dao;

import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.Country;

/**
 * Created by ravirane on 8/17/15.
 */
public class CountryDao extends BaseDao {


    public CountryDao(Context context) {
        super(context);
    }

    public static Country saveCountry(Country country) {
        if (country != null) {
            String inserQuery = "INSERT OR REPLACE INTO COUNTRY(ctryName, agreement) VALUES('" + country.ctryName +
                    "','" + country.agreementState + "')";
            db.execSQL(inserQuery);
        }
        return country;
    }

    public static Country getCountry() {
        Cursor cursor = db.rawQuery("SELECT * FROM COUNTRY", null);
        Country country = null;
        if (cursor.moveToFirst()) {
            country = new Country(cursor.getString(0), cursor.getString(1));
        }
        return country;
    }

    public static void clearCountry() {
        String deletQuery = "DELETE FROM COUNTRY";
        db.execSQL(deletQuery);
    }

}
