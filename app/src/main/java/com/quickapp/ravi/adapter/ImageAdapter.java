package com.quickapp.ravi.adapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by ravirane on 7/13/15.
 */
public class ImageAdapter extends PagerAdapter {

    Context context;

    private List<String> imgString;

    public ImageAdapter(Context context, List<String> imgString) {
        this.context = context;
        this.imgString = imgString;
    }

    @Override
    public int getCount() {
        return imgString.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);

        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Glide.with(imageView.getContext())
                .load(imgString.get(position))
                .fitCenter()
                .into(imageView);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }
}

