package com.quickapp.ravi.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/19/15.
 */
public class CardDao extends BaseDao{

    public CardDao(Context context) {
        super(context);
    }

    public static void clearCards() {
        String deletQuery = "DELETE FROM CARD";
        db.execSQL(deletQuery);
    }


    public static List<Card> getCards(Context context, int bankIdin) {
        List<Card> cards = new ArrayList<>();

        open(context);
        String selectQuery = "select cardId,card.bankId as bankId,cardName,cardScore,cardPic,annualFeesPrincipal,annualFeesSupplementary,feeWaiver,age," +
                "minIncomeLocal,minIncomeForeiger,balanceTxInterestRate,balanceTxMaxAmount,tier1features,tier2features," +
                "tier3features, bank.bankContactNo as bankContactNo from CARD card, BANK bank where card.bankId = bank.bankId and card.bankId = " + bankIdin;

        Cursor mCursor = db.rawQuery(selectQuery, null);

        if (mCursor.moveToFirst()) {
            do {
                Card card = new Card();

                card.cardId = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardId"));
                card.bankId = mCursor.getInt(mCursor.getColumnIndexOrThrow("bankId"));
                card.cardName = mCursor.getString(mCursor.getColumnIndexOrThrow("cardName"));
                card.cardScore = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardScore"));
                card.cardPic = mCursor.getString(mCursor.getColumnIndexOrThrow("cardPic"));
                card.annualFeesPrincipal = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesPrincipal"));
                card.annualFeesSupplementary = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesSupplementary"));
                card.feeWaiver = mCursor.getInt(mCursor.getColumnIndexOrThrow("feeWaiver"));
                card.age = mCursor.getInt(mCursor.getColumnIndexOrThrow("age"));
                card.minIncomeLocal = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeLocal"));
                card.minIncomeForeiger = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeForeiger"));
                card.balanceTxInterestRate = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxInterestRate"));
                card.balanceTxMaxAmount = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxMaxAmount"));
                card.tier1features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier1features"));
                card.tier2features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier2features"));
                card.tier3features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier3features"));
                card.contactNo = mCursor.getString(mCursor.getColumnIndexOrThrow("bankContactNo"));

                cards.add(card);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return cards;
    }

    public static List<Card> getCards(Context context) {
        List<Card> cards = new ArrayList<>();

        open(context);
        String selectQuery = "select cardId,card.bankId as bankId,cardName,cardScore,cardPic,annualFeesPrincipal,annualFeesSupplementary,feeWaiver,age," +
                "minIncomeLocal,minIncomeForeiger,balanceTxInterestRate,balanceTxMaxAmount,tier1features,tier2features," +
                "tier3features, bank.bankContactNo from CARD card, BANK bank where card.bankId = bank.bankId";

        Cursor mCursor = db.rawQuery(selectQuery, null);

        if (mCursor.moveToFirst()) {
            do {
                Card card = new Card();

                card.cardId = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardId"));
                card.bankId = mCursor.getInt(mCursor.getColumnIndexOrThrow("bankId"));
                card.cardName = mCursor.getString(mCursor.getColumnIndexOrThrow("cardName"));
                card.cardScore = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardScore"));
                card.cardPic = mCursor.getString(mCursor.getColumnIndexOrThrow("cardPic"));
                card.annualFeesPrincipal = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesPrincipal"));
                card.annualFeesSupplementary = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesSupplementary"));
                card.feeWaiver = mCursor.getInt(mCursor.getColumnIndexOrThrow("feeWaiver"));
                card.age = mCursor.getInt(mCursor.getColumnIndexOrThrow("age"));
                card.minIncomeLocal = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeLocal"));
                card.minIncomeForeiger = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeForeiger"));
                card.balanceTxInterestRate = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxInterestRate"));
                card.balanceTxMaxAmount = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxMaxAmount"));
                card.tier1features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier1features"));
                card.tier2features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier2features"));
                card.tier3features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier3features"));
                card.contactNo = mCursor.getString(mCursor.getColumnIndexOrThrow("bankContactNo"));

                cards.add(card);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return cards;
    }


    public static  void saveCards(List<Card> cards) {
        for(Card card : cards) {
            ContentValues values = new ContentValues();
            values.put("cardId",card.cardId);
            values.put("bankId",card.bankId);
            values.put("cardName",card.cardName);
            values.put("cardScore",card.cardScore);
            values.put("cardPic",card.cardPic);
            values.put("annualFeesPrincipal",card.annualFeesPrincipal);
            values.put("annualFeesSupplementary",card.annualFeesSupplementary);
            values.put("feeWaiver",card.feeWaiver);
            values.put("age",card.age);
            values.put("minIncomeLocal",card.minIncomeLocal);
            values.put("minIncomeForeiger",card.minIncomeForeiger);
            values.put("balanceTxInterestRate",card.balanceTxInterestRate);
            values.put("balanceTxMaxAmount",card.balanceTxMaxAmount);
            values.put("tier1features",card.tier1features);
            values.put("tier2features",card.tier2features);
            values.put("tier3features",card.tier3features);
            db.insert("CARD",null,values);
        }
    }
}
