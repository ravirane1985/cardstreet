package com.quickapp.ravi.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ravirane on 7/12/15.
 */
public class LoadImageTaskOld extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView = null;

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {
        this.imageView = imageViews[0];
        return downloadImage((String) imageView.getTag());
    }

    private Bitmap downloadImage(String imgLocation) {
        URL imageURL = null;
        if (imgLocation != null) {
            try {
                imageURL = new URL(imgLocation);
                HttpURLConnection connection = (HttpURLConnection) imageURL
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream inputStream = connection.getInputStream();

                BitmapFactory.Options opts=new BitmapFactory.Options();
                opts.inDither=false;                     //Disable Dithering mode
                opts.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
                opts.inInputShareable=true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
                opts.inTempStorage=new byte[32 * 1024];


                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);// Convert to bitmap



                return bitmap;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //set any default
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }
}
