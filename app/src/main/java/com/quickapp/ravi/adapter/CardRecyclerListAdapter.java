package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickapp.ravi.cardstreet.CardDetailsActivity;
import com.quickapp.ravi.cardstreet.R;
import com.quickapp.ravi.model.Card;

import java.util.List;

/**
 * Created by ravirane on 8/10/15.
 */
public class CardRecyclerListAdapter extends RecyclerView.Adapter<CardRecyclerListAdapter.CardViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<Card> cardList;
    String parent;
    private Integer bankId = null;

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        public String mBoundString;

        public final View mView;
        public final TextView txtCardName;
        public final TextView txtCardChargeDetails;
        public final TextView txtCardScore;

        public CardViewHolder(View view) {
            super(view);
            mView = view;
            txtCardName = (TextView) view.findViewById(R.id.cardName);
            txtCardChargeDetails = (TextView) view.findViewById(R.id.cardChargeDetails);
            txtCardScore = (TextView) view.findViewById(R.id.cardScore);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtCardName.getText();
        }
    }

    public Card getValueAt(int position) {
        return cardList.get(position);
    }

    public CardRecyclerListAdapter(Context context, List<Card> cardList, String parent, int bankId) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.cardList = cardList;
        this.parent = parent;
        this.bankId = bankId;
    }

    public CardRecyclerListAdapter(Context context, List<Card> cardList, String parent) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.cardList = cardList;
        this.parent = parent;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        final Card card = cardList.get(position);
        holder.txtCardName.setText(card.cardName);
        holder.txtCardScore.setText("Score : " + card.cardScore);
        holder.txtCardChargeDetails.setText(getCardChargeDetails(card));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, CardDetailsActivity.class);
                intent.putExtra("parent",parent);
                intent.putExtra("card", card);
                intent.putExtra("bankId", bankId);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    private String getCardChargeDetails(Card card) {
        String cardCharge = null;

        if(card.annualFeesPrincipal == 0) {
            if(card.feeWaiver == 0) {
                cardCharge = "This card is free for Lifetime";
            } else {
                cardCharge = "This card is free for " + card.feeWaiver;
            }

        } else {
            cardCharge = "Annual fees for this card is : " + card.annualFeesPrincipal;
        }

        return cardCharge;
    }
}
