package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quickapp.ravi.cardstreet.CardDetailsActivity;
import com.quickapp.ravi.cardstreet.R;
import com.quickapp.ravi.model.Promotion;

import java.util.List;

/**
 * Created by ravirane on 8/13/15.
 */
public class PromotionRecyclerListAdapter extends RecyclerView.Adapter<PromotionRecyclerListAdapter.PromoViewHolder>{

    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<Promotion> promotionList;
    String parent;

    public static class PromoViewHolder extends RecyclerView.ViewHolder {
        public String mBoundString;

        public final View mView;
        public final ImageView imgPromo;
        public final TextView txtPromoHeader;
        public final TextView txtPromoShortDetail;



        public PromoViewHolder(View view) {
            super(view);
            mView = view;
            imgPromo = (ImageView)view.findViewById(R.id.imgPromo);
            txtPromoHeader = (TextView) view.findViewById(R.id.txtPromoHeading);
            txtPromoShortDetail = (TextView) view.findViewById(R.id.txtPromoDetails);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtPromoHeader.getText();
        }
    }


    public Promotion getValueAt(int position) {
        return promotionList.get(position);
    }

    public PromotionRecyclerListAdapter(Context context, List<Promotion> promoList, String parent) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.promotionList = promoList;
        this.parent = parent;
    }

    @Override
    public PromoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promo_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new PromoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PromoViewHolder holder, int position) {
        Promotion promotion = promotionList.get(position);
        holder.txtPromoHeader.setText(promotion.promoHeader);
        holder.txtPromoShortDetail.setText(promotion.promoShortDetails);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, CardDetailsActivity.class);
                intent.putExtra("parent",parent);
              /*  intent.putExtra("parent",parent);
                intent.putExtra("card", card);
                intent.putExtra("bankId", bankId);*/
                context.startActivity(intent);
            }
        });

        Glide.with(holder.imgPromo.getContext())
                .load(promotion.promoLogo)
                .fitCenter()
                .into(holder.imgPromo);
    }

    @Override
    public int getItemCount() {
        return promotionList.size();
    }

}
