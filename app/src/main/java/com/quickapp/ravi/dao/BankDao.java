package com.quickapp.ravi.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.quickapp.ravi.model.Bank;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/16/15.
 */
public class BankDao  extends BaseDao{


    private static String tableName = "BANK";

    public BankDao(Context context) {
        super(context);
    }


    public void populateBankDb() {

    }

    public static void saveBanks(List<Bank> banks) {
        for(Bank bank : banks) {
            ContentValues values = new ContentValues();
            values.put("bankName",bank.bankName );
            values.put("bankId",bank.bankId);
            values.put("bankLogo",bank.bankLogo);
            values.put("bankContactNo",bank.bankContactNo);
            values.put("country",bank.country);
            db.insert("BANK",null,values);
        }
    }

    public static void clearBanks() {
        String deletQuery = "DELETE FROM BANK";
        db.execSQL(deletQuery);
    }

    public static  List<Bank> getBanks(Context context) {
        open(context);
        List<Bank> bankLists = new ArrayList<>();

        Cursor mCursor = db.query(tableName,null , null,null,
                        null, null, null , null);

        if (mCursor.moveToFirst()) {
            do {
                Bank bank = new Bank();
                bank.bankId = mCursor.getInt(mCursor.getColumnIndexOrThrow("bankId"));
                bank.bankName = mCursor.getString(mCursor.getColumnIndexOrThrow("bankName"));
                bank.bankLogo =  mCursor.getString(mCursor.getColumnIndexOrThrow("bankLogo"));
                bank.bankContactNo =  mCursor.getString(mCursor.getColumnIndexOrThrow("bankContactNo"));
                bank.country =   mCursor.getString(mCursor.getColumnIndexOrThrow("country"));
                bankLists.add(bank);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return bankLists;
    }

    public static  List<Bank> getBanksWithCards(Context context) {
        open(context);
        List<Bank> bankLists = new ArrayList<>();


        String selectQuery = "SELECT bank.bankId,bankName,bankLogo,bankContactNo,country,count(*) as cardCount from BANK bank, CARD card " +
                "where bank.bankId = card.bankId group by bank.bankId,bankName,bankLogo,bankContactNo,country";
         Log.e(selectQuery,selectQuery);

        Cursor mCursor = db.rawQuery(selectQuery, null);


        if (mCursor.moveToFirst()) {
            do {
                Bank bank = new Bank();
                bank.bankId = mCursor.getInt(mCursor.getColumnIndexOrThrow("bankId"));
                bank.bankName = mCursor.getString(mCursor.getColumnIndexOrThrow("bankName"));
                bank.bankLogo =  mCursor.getString(mCursor.getColumnIndexOrThrow("bankLogo"));
                bank.bankContactNo =  mCursor.getString(mCursor.getColumnIndexOrThrow("bankContactNo"));
                bank.country =   mCursor.getString(mCursor.getColumnIndexOrThrow("country"));
                bank.cardCount = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardCount"));
                bankLists.add(bank);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return bankLists;
    }

}
