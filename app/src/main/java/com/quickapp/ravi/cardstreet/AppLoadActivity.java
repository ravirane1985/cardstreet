package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView;
import com.quickapp.ravi.dao.BankDao;
import com.quickapp.ravi.dao.CardDao;
import com.quickapp.ravi.dao.CountryDao;
import com.quickapp.ravi.data.BankData;
import com.quickapp.ravi.model.Bank;
import com.quickapp.ravi.model.Card;
import com.quickapp.ravi.model.Country;
import com.quickapp.ravi.util.DDLHelper;
import com.quickapp.ravi.util.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class AppLoadActivity extends AppCompatActivity {

    private AnimatedCircleLoadingView animatedCircleLoadingView;
    private DatabaseHelper databaseHelper;
    private boolean ddlGood = false;
    private boolean dataGood = false;
    private boolean webServiceGood = false;
    private boolean emptyTable = false;
    private boolean loadedTables = false;
    List<Bank> banks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_load);
        animatedCircleLoadingView = (AnimatedCircleLoadingView) findViewById(R.id.circle_loading_view);

        Button btnempty = (Button) findViewById(R.id.emptydb);
        btnempty.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CountryDao.open(AppLoadActivity.this);
                                            CountryDao.clearCountry();
                                            CountryDao.close();
                                        }
                                    }
        );


        DDLHelper.createCountryTable(getApplicationContext());

        CountryDao.open(AppLoadActivity.this);
        Country ctry = CountryDao.getCountry();
        CountryDao.close();
        if(ctry == null) {
            Intent intent = new Intent(AppLoadActivity.this, AgreementActivity.class);
            AppLoadActivity.this.startActivity(intent);
        } else {
            new CircleLoaderTask().execute("http://cardstreet-realquickapp.rhcloud.com/cards");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app_load, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class CircleLoaderTask extends AsyncTask<String, Integer, Void> {
        @Override
        protected void onPreExecute() {
            animatedCircleLoadingView.startDeterminate();
        }

        @Override
        protected Void doInBackground(String... urls) {

            setPercent(0);
            verifyDb();
            setPercent(20);


           String jsonResp = null;
         /*  HTTPConnect connect = new HTTPConnect();
            jsonResp = connect.GET(urls[0]);*/



            setPercent(40);

            if(jsonResp != null) {
                banks = getBankData(jsonResp);
            } else {
                banks = getBankData(BankData.getBankJson2());
            }
            clearTable(banks);
            setPercent(60);
            saveDataInTable(banks);
            setPercent(80);

            return null;
        }

        private void saveDataInTable(List<Bank> banks) {
            if(banks != null && banks.size() > 0) {
                BankDao.open(AppLoadActivity.this);
                BankDao.saveBanks(banks);
                BankDao.close();
                for(Bank bank : banks) {
                    CardDao.open(AppLoadActivity.this);
                    CardDao.saveCards(bank.cards);
                    CardDao.close();
                }
            }
        }

        private void clearTable(List<Bank> banks) {
            if(banks != null && banks.size() > 0) {
                BankDao.open(AppLoadActivity.this);
                BankDao.clearBanks();
                BankDao.close();
                CardDao.open(AppLoadActivity.this);
                CardDao.clearCards();
                CardDao.close();
            }
        }

        private List<Bank> getBankData(String bankJson) {


            List<Bank> banks = new ArrayList<>();
            try {
                JSONObject jsonRootObject = new JSONObject(bankJson);
                JSONArray jsonArray = jsonRootObject.optJSONArray("bank");
                for(int i = 0; i < jsonArray.length(); i++ ) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Bank bank = new Bank(jsonObject.optString("bankName"),
                            jsonObject.optInt("bankId"), jsonObject.optString("bankLogo"),
                            jsonObject.optString("bankContactNo"), jsonObject.optString("country"));

                    List<Card> cards = getCards(jsonObject);
                    bank.cards = cards;
                    banks.add(bank);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return banks;
        }

        private List<Card> getCards(JSONObject jsonObject) {
            List<Card> cards = new ArrayList<>();
            int bankId = jsonObject.optInt("bankId");

            try {
                JSONArray jsonArray = jsonObject.optJSONArray("cards");
                for(int i = 0; i < jsonArray.length(); i++ ) {
                    JSONObject cardJson = jsonArray.getJSONObject(i);
                    Card card = new Card(cardJson.optInt("cardId") , cardJson.optString("cardName") , cardJson.optInt("cardScore") ,
                            cardJson.optString("cardPic"), cardJson.optInt("annualFeesPrincipal") , cardJson.optInt("annualFeesSupplementary") ,
                            cardJson.optInt("feeWaiver") , cardJson.optInt("age") , cardJson.optInt("minIncomeLocal") ,
                            cardJson.optInt("minIncomeForeiger") , cardJson.optInt("balanceTxInterestRate") , cardJson.optInt("balanceTxMaxAmount") ,
                            cardJson.optString("tier1features") , cardJson.optString("tier2features") , cardJson.optString("tier3features") );
                    card.bankId = bankId;
                    cards.add(card);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return cards;
        }
/*
        private List<String> getCardPics(JSONObject cardJson) {
            List<String> cardPics = new ArrayList<>();
            String cardPicString = cardJson.optString("cardPic");
            cardPics = Arrays.asList(cardPicString.split(","));
            return cardPics;
        }*/


        public void verifyDb() {
            DDLHelper ddlHelper = new DDLHelper(AppLoadActivity.this);
            ddlHelper.open(AppLoadActivity.this);
            ddlHelper.createBankTable();
            ddlHelper.createCardTable();
            ddlHelper.createProfileTable();
            ddlHelper.createWalletTable();
            ddlHelper.createInterestTable();
            ddlHelper.close();
        }

        public void setPercent(int per) {
                try {
                    for (int i = per + 1; i <= per + 20; i++) {
                        Thread.sleep(65);
                        publishProgress(i);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = new Intent(AppLoadActivity.this, FIDashBoardActivity.class);
            intent.putExtra("tabNo", 0);
            AppLoadActivity.this.startActivity(intent);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
                    animatedCircleLoadingView.setPercent(values[0]);
            }
    }
}
