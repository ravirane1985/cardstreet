package com.quickapp.ravi.cardstreet;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.adapter.PromotionRecyclerListAdapter;
import com.quickapp.ravi.data.PromoData;

/**
 * Created by ravirane on 8/8/15.
 */
public class PromotionFragment extends Fragment{


    public PromotionFragment() {
    }

    public static PromotionFragment newInstance(String str) {
        PromotionFragment promotionFragment = new PromotionFragment();
        return promotionFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView rv = (RecyclerView) inflater.inflate(
                R.layout.fragment_promo, container, false);
        setupRecyclerView(rv);
        return rv;

    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new PromotionRecyclerListAdapter(getActivity(),
                PromoData.getPromoData(), "promofragment"));
    }


}
