package com.quickapp.ravi.data;

import com.quickapp.ravi.model.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/10/15.
 */
public class CardData {

    public static List<Card> getCardData() {
        List<Card> cardList = new ArrayList<>();
        getCardList(cardList);
        return cardList;
    }

    private static void getCardList(List<Card> cardList) {

        Card c1 = new Card(1,"DBS Black Visa Card", 200, 200, 0);
        Card c2 = new Card(2,"DBS Live Fresh Visa Card", 160, 0, 3);
        Card c3 = new Card(3,"DBS Altitude Visa Signature Card", 170, 100, 0);
        Card c4 = new Card(4,"DBS Woman's World MasterCard® Card", 155, 0, 2);
        Card c5 = new Card(5,"DBS Black American Express® Card", 120, 50, 0);
        Card c6 = new Card(6,"DBS Woman's World MasterCard® Card", 130, 40, 0);
        Card c7 = new Card(7,"DBS Esso MasterCard® Card", 112, 100, 0);
        Card c8 = new Card(8,"DBS NUSS Visa Card", 135, 0, 0);
        Card c9 = new Card(9,"1 DBS Takashimaya American Express® Card", 140, 100, 0);
        Card c10 = new Card(10,"2 DBS Takashimaya American Express® Card", 140, 100, 0);
        Card c11 = new Card(11,"3 DBS Takashimaya American Express® Card", 140, 100, 0);
        Card c12 = new Card(12,"4 DBS Takashimaya American Express® Card", 140, 100, 0);
        cardList.add(c1);
        cardList.add(c2);
        cardList.add(c3);
        cardList.add(c4);
        cardList.add(c5);
        cardList.add(c6);
        cardList.add(c7);
        cardList.add(c8);
        cardList.add(c9);
        cardList.add(c10);
        cardList.add(c11);
        cardList.add(c12);
    }


}
