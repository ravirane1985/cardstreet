package com.quickapp.ravi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quickapp.ravi.cardstreet.BankCardActivity;
import com.quickapp.ravi.cardstreet.R;
import com.quickapp.ravi.model.Bank;

import java.util.List;

/**
 * Created by ravirane on 8/11/15.
 */
public class BankRecyclerListAdapter extends RecyclerView.Adapter<BankRecyclerListAdapter.BankViewHolder> {


    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<Bank> bankList;
    private Context context;

    public static class BankViewHolder extends RecyclerView.ViewHolder {
        public String mBoundString;

        public final View mView;
        public final ImageView bankLogo;
        public final TextView txtCardCount;


        public BankViewHolder(View view) {
            super(view);
            mView = view;
            bankLogo = (ImageView)view.findViewById(R.id.bankLogo);
            txtCardCount = (TextView) view.findViewById(R.id.txtCardCount);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtCardCount.getText();
        }
    }


    public Bank getValueAt(int position) {
        return bankList.get(position);
    }

    public BankRecyclerListAdapter(Context context, List<Bank> bankList) {
        this.context = context;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.bankList = bankList;
    }

    @Override
    public BankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_row, parent, false);
        view.setBackgroundResource(mBackground);
        return new BankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BankViewHolder holder,final int position) {
        final Bank bank = bankList.get(position);
        holder.txtCardCount.setText(Integer.toString(bank.cardCount) + " Cards");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, BankCardActivity.class);
                Log.e("Bank :" + bank.bankId,"Bank : " + bank.bankId);
                intent.putExtra("bankId", bank.bankId);
                context.startActivity(intent);
            }
        });

       Glide.with(holder.bankLogo.getContext())
                .load(bank.bankLogo)
                .fitCenter()
                .into(holder.bankLogo);
    }

    @Override
    public int getItemCount() {
        return bankList.size();
    }


}
