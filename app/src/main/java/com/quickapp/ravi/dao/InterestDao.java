package com.quickapp.ravi.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.quickapp.ravi.model.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravirane on 8/20/15.
 */
public class InterestDao extends BaseDao {

    public InterestDao(Context context) {
        super(context);
    }

    public static List<Card> getInterestCards(Context context) {

        List<Card> cards = new ArrayList<>();

        open(context);
        String selectQuery = "select card.cardId,card.bankId as bankId,cardName,cardScore,cardPic,annualFeesPrincipal,annualFeesSupplementary,feeWaiver,age," +
                "minIncomeLocal,minIncomeForeiger,balanceTxInterestRate,balanceTxMaxAmount,tier1features,tier2features," +
                "tier3features, bank.bankContactNo as bankContactNo from CARD card, BANK bank, INTEREST interest where card.bankId = bank.bankId " +
                " and interest.cardId = card.cardId";

        Cursor mCursor = db.rawQuery(selectQuery, null);

        if (mCursor.moveToFirst()) {
            do {
                Card card = new Card();

                card.cardId = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardId"));
                card.bankId = mCursor.getInt(mCursor.getColumnIndexOrThrow("bankId"));
                card.cardName = mCursor.getString(mCursor.getColumnIndexOrThrow("cardName"));
                card.cardScore = mCursor.getInt(mCursor.getColumnIndexOrThrow("cardScore"));
                card.cardPic = mCursor.getString(mCursor.getColumnIndexOrThrow("cardPic"));
                card.annualFeesPrincipal = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesPrincipal"));
                card.annualFeesSupplementary = mCursor.getInt(mCursor.getColumnIndexOrThrow("annualFeesSupplementary"));
                card.feeWaiver = mCursor.getInt(mCursor.getColumnIndexOrThrow("feeWaiver"));
                card.age = mCursor.getInt(mCursor.getColumnIndexOrThrow("age"));
                card.minIncomeLocal = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeLocal"));
                card.minIncomeForeiger = mCursor.getInt(mCursor.getColumnIndexOrThrow("minIncomeForeiger"));
                card.balanceTxInterestRate = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxInterestRate"));
                card.balanceTxMaxAmount = mCursor.getInt(mCursor.getColumnIndexOrThrow("balanceTxMaxAmount"));
                card.tier1features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier1features"));
                card.tier2features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier2features"));
                card.tier3features = mCursor.getString(mCursor.getColumnIndexOrThrow("tier3features"));
                card.contactNo = mCursor.getString(mCursor.getColumnIndexOrThrow("bankContactNo"));

                cards.add(card);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return cards;

    }


    public static void saveInterestCard(Context context, int cardId) {
        open(context);
        ContentValues values = new ContentValues();
        values.put("cardId",cardId);
        db.insert("INTEREST", null, values);
        close();
    }


    public static boolean isCardAvailable(Context context, int cardId) {
        boolean flag = false;
        open(context);
        String selectQuery = "select 1 from INTEREST where cardId = " + cardId;

        Cursor mCursor = db.rawQuery(selectQuery, null);

        if (mCursor.moveToFirst()) {
            flag = true;
        }

        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        close();
        return flag;
    }

    public static void removeFromInterest(Context context, int cardId) {
        open(context);
        String deleteQuery = "delete from INTEREST where cardId = " + cardId;

        db.execSQL(deleteQuery);

        close();
    }

}
