package com.quickapp.ravi.model;

import java.io.Serializable;

/**
 * Created by ravirane on 8/10/15.
 */
public class Card implements Serializable {
    public int cardId;
    public int bankId;
    public String cardName;
    public int cardScore;
    public String cardPic;
    public int annualFeesPrincipal;
    public int annualFeesSupplementary;
    public int feeWaiver;
    public int age;
    public int minIncomeLocal;
    public int minIncomeForeiger;
    public int balanceTxInterestRate;
    public int balanceTxMaxAmount;
    public String tier1features;
    public String tier2features;
    public String tier3features;
    public String contactNo;
    public String billingDate;

    public Card() {
    }

    public Card(int cardId, String cardName, int cardScore, int annualFeesPrincipal, int feeWaiver) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.cardScore = cardScore;
        this.annualFeesPrincipal = annualFeesPrincipal;
        this.feeWaiver = feeWaiver;
    }

    public Card(int cardId, String cardName, int cardScore, String cardPic, int annualFeesPrincipal, int annualFeesSupplementary, int feeWaiver, int age, int minIncomeLocal, int minIncomeForeiger, int balanceTxInterestRate, int balanceTxMaxAmount, String tier1features, String tier2features, String tier3features) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.cardScore = cardScore;
        this.cardPic = cardPic;
        this.annualFeesPrincipal = annualFeesPrincipal;
        this.annualFeesSupplementary = annualFeesSupplementary;
        this.feeWaiver = feeWaiver;
        this.age = age;
        this.minIncomeLocal = minIncomeLocal;
        this.minIncomeForeiger = minIncomeForeiger;
        this.balanceTxInterestRate = balanceTxInterestRate;
        this.balanceTxMaxAmount = balanceTxMaxAmount;
        this.tier1features = tier1features;
        this.tier2features = tier2features;
        this.tier3features = tier3features;
    }
}
