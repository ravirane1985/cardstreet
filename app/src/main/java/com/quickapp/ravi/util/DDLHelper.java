package com.quickapp.ravi.util;

import android.content.Context;

import com.quickapp.ravi.dao.BaseDao;

/**
 * Created by ravirane on 8/16/15.
 */
public class DDLHelper extends BaseDao {

    public DDLHelper(Context context) {
        super(context);
    }

    public void createProfileTable() {
        String profile = "CREATE TABLE IF NOT EXISTS PROFILE( profileName TEXT PRIMARY KEY, mailId TEXT, age TEXT, monthlyIncome TEXT, contact TEXT )";
        db.execSQL(profile);
    }

    public void createBankTable() {
        String bank = "CREATE TABLE IF NOT EXISTS BANK(bankId INTEGER PRIMARY KEY, bankName TEXT, bankLogo TEXT, bankContactNo TEXT, country TEXT)";
        db.execSQL(bank);
    }

    public void createCardTable() {
        String card = "CREATE TABLE IF NOT EXISTS CARD(cardId INTEGER PRIMARY KEY,bankId INTEGER, cardName TEXT, cardScore INTEGER, cardPic TEXT" +
        ", annualFeesPrincipal INTEGER, annualFeesSupplementary INTEGER, feeWaiver INTEGER, age INTEGER, minIncomeLocal INTEGER" +
        ", minIncomeForeiger INTEGER, balanceTxInterestRate INTEGER, balanceTxMaxAmount INTEGER, tier1features TEXT, tier2features TEXT," +
                " tier3features TEXT )";
        db.execSQL(card);
    }


    public void createWalletTable() {
        String wallet = "CREATE TABLE IF NOT EXISTS WALLET(cardId INTEGER PRIMARY KEY, billingDate TEXT)";
        db.execSQL(wallet);
    }

    public void createInterestTable() {
        String wallet = "CREATE TABLE IF NOT EXISTS INTEREST(cardId INTEGER PRIMARY KEY)";
        db.execSQL(wallet);
    }

    public static void createCountryTable(Context context) {
        open(context);
        String ctryString = "CREATE TABLE IF NOT EXISTS COUNTRY(ctryName TEXT PRIMARY KEY, agreement TEXT)";
        db.execSQL(ctryString);
        close();
    }
}
