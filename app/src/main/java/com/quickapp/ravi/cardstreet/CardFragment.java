package com.quickapp.ravi.cardstreet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quickapp.ravi.adapter.CardRecyclerListAdapter;
import com.quickapp.ravi.dao.CardDao;

/**
 * Created by ravirane on 8/8/15.
 */
public class CardFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    public CardFragment() {
        super();
    }

    public static CardFragment newInstance(String str) {
        CardFragment cardFragment = new CardFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_PARAM1, str);
        cardFragment.setArguments(bundle);
        return cardFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView rv = (RecyclerView) inflater.inflate(
                R.layout.fragment_card, container, false);
        setupRecyclerView(rv);
        return rv;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new CardRecyclerListAdapter(getActivity(),
                CardDao.getCards(recyclerView.getContext()),"cardfragment"));
    }

}
