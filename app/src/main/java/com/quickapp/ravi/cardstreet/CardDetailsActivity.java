package com.quickapp.ravi.cardstreet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.quickapp.ravi.adapter.CardDetailsRecyclerAdapter;
import com.quickapp.ravi.adapter.ImageAdapter;
import com.quickapp.ravi.dao.InterestDao;
import com.quickapp.ravi.dao.WalletDao;
import com.quickapp.ravi.model.Card;

import java.util.ArrayList;
import java.util.List;


public class CardDetailsActivity extends AppCompatActivity {

    private List<String> imgString = new ArrayList<>();

    private RecyclerView bankCardDetailsView;
    private CardDetailsRecyclerAdapter cardDetailsRecyclerAdapter;
    List<String> cardDetails;
    String parent;
    int bankId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);

        Intent intent = getIntent();
        parent = intent.getStringExtra("parent");

        final Card card = (Card) intent.getSerializableExtra("card");

        final String contactNo = card.contactNo;
        final Integer cardId = card.cardId;

        bankId = intent.getIntExtra("bankId", 0);

        cardDetails = new ArrayList<>();

        setCardDetails(cardDetails, card);

        populateImgString();

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        ImageAdapter adapter = new ImageAdapter(this,imgString);
        viewPager.setAdapter(adapter);


        // Initialize recycler view
        bankCardDetailsView = (RecyclerView) findViewById(R.id.cardDetailsRecyclerView);
        bankCardDetailsView.setLayoutManager(new LinearLayoutManager(this));
        cardDetailsRecyclerAdapter = new CardDetailsRecyclerAdapter(CardDetailsActivity.this, cardDetails);
        bankCardDetailsView.setAdapter(cardDetailsRecyclerAdapter);


        Button btnCallMe = (Button) findViewById(R.id.btnCallMe);
        btnCallMe.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             Intent intent = new Intent(Intent.ACTION_DIAL);
                                             intent.setData(Uri.parse("tel:" + contactNo));
                                             startActivity(intent);
                                         }
                                     }
        );

       final Button btnAddInWallet = (Button) findViewById(R.id.btnAddWallet);
       final Button btnInterested = (Button) findViewById(R.id.btnInterested);

       final Button btnBillingDate = (Button) findViewById(R.id.btnBillingDate);
        Button btnRemove = (Button) findViewById(R.id.btnRemoveFromWallet);

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.btnFloatingAction);
        floatingActionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(CardDetailsActivity.this);
                View promptsView = li.inflate(R.layout.feedback, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        CardDetailsActivity.this);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);


                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });





        if(WalletDao.isCardAvailable(getApplicationContext(),cardId)) {
            btnAddInWallet.setEnabled(false);
            btnAddInWallet.setBackgroundColor(Color.DKGRAY);
            btnInterested.setEnabled(false);
            btnInterested.setBackgroundColor(Color.DKGRAY);
        }

        if(InterestDao.isCardAvailable(getApplicationContext(),cardId)){
            btnInterested.setEnabled(false);
            btnInterested.setBackgroundColor(Color.DKGRAY);
        }


        if("walletcardactivity".equals(parent)) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.bottomLayout);
            linearLayout.setVisibility(View.INVISIBLE);
            linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutWallet);
            linearLayout.setVisibility(View.VISIBLE);
            if(card.billingDate != null && card.billingDate.length() > 0) {
                btnBillingDate.setText(card.billingDate);
            }
        }  else {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.bottomLayout);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout = (LinearLayout) findViewById(R.id.bottomLayoutWallet);
            linearLayout.setVisibility(View.INVISIBLE);
        }



        btnAddInWallet.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             WalletDao.saveWalletCard(getApplicationContext(), cardId);
                                             InterestDao.removeFromInterest(getApplicationContext(), cardId);
                                             btnAddInWallet.setEnabled(false);
                                             btnAddInWallet.setBackgroundColor(Color.DKGRAY);
                                             btnInterested.setEnabled(false);
                                             btnInterested.setBackgroundColor(Color.DKGRAY);
                                             Intent intent = new Intent(CardDetailsActivity.this, WalletCardActivity.class);
                                             CardDetailsActivity.this.startActivity(intent);
                                         }
                                     }
        );



        btnInterested.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             InterestDao.saveInterestCard(getApplicationContext(), cardId);
                                             btnInterested.setEnabled(false);
                                             btnInterested.setBackgroundColor(Color.DKGRAY);
                                         }
                                     }
        );


        btnBillingDate.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {

                                                 // get prompts.xml view
                                                 LayoutInflater li = LayoutInflater.from(CardDetailsActivity.this);
                                                 View promptsView = li.inflate(R.layout.prompt, null);

                                                 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                                         CardDetailsActivity.this);

                                                 // set prompts.xml to alertdialog builder
                                                 alertDialogBuilder.setView(promptsView);

                                                 final EditText txtBillingDate = (EditText) promptsView
                                                         .findViewById(R.id.txtBillingDate);

                                                 // set dialog message
                                                 alertDialogBuilder
                                                         .setCancelable(false)
                                                         .setPositiveButton("OK",
                                                                 new DialogInterface.OnClickListener() {
                                                                     public void onClick(DialogInterface dialog,int id) {
                                                                         if(txtBillingDate.getText() != null &&
                                                                                 txtBillingDate.getText().length() > 0){
                                                                             WalletDao.updateBillingDateForCard(getApplicationContext(),
                                                                                     cardId,txtBillingDate.getText().toString());
                                                                             btnBillingDate.setText(txtBillingDate.getText());
                                                                         }
                                                                     }
                                                                 })
                                                         .setNegativeButton("Cancel",
                                                                 new DialogInterface.OnClickListener() {
                                                                     public void onClick(DialogInterface dialog, int id) {
                                                                         dialog.cancel();
                                                                     }
                                                                 });

                                                 // create alert dialog
                                                 AlertDialog alertDialog = alertDialogBuilder.create();

                                                 // show it
                                                 alertDialog.show();

                                             }
                                         }
        );


        btnRemove.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 WalletDao.removeFromWallet(getApplicationContext(),cardId);
                                                 Intent intent = new Intent(CardDetailsActivity.this, WalletCardActivity.class);
                                                 CardDetailsActivity.this.startActivity(intent);
                                             }
                                         }
        );


    }

    private void setCardDetails(List<String> cardDetails, Card card) {

        String minReq = null;

        if(card.tier1features != null && card.tier1features.length() > 0) {
            cardDetails.add(card.tier1features);
        }
        if(card.tier2features != null && card.tier2features.length() > 0) {
            cardDetails.add(card.tier2features);
        }
        if(card.tier3features != null && card.tier3features.length() > 0) {
            cardDetails.add(card.tier3features);
        }
        if(card.age != 0) {
            minReq = "Min Age : " + card.age + "\n";
        }

        if(card.minIncomeForeiger != 0) {
            minReq = minReq + " Min Foreigner income :" +  card.minIncomeForeiger + "\n";
        }

        if(card.minIncomeLocal != 0) {
            minReq = minReq + " Min Resident income :" +  card.minIncomeLocal + "\n";
        }
        cardDetails.add(minReq);

        if(card.annualFeesPrincipal != 0) {
            cardDetails.add("Annual Fee for main card: " + card.annualFeesPrincipal);
        } else {
            cardDetails.add("There is no annual Fee for main card");
        }

        if(card.annualFeesSupplementary != 0) {
            cardDetails.add("Annual Fee for supplementary card: " + card.annualFeesSupplementary);
        } else {
            cardDetails.add("There is no annual Fee for supplementary card");
        }

    }

    private void populateImgString() {
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436768040/singapore_rqdh0j.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767999/singapore4_sozhjb.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767996/singapore3_hx8apl.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767992/singapore2_z8kwo2.jpg");
        imgString.add("http://res.cloudinary.com/djh5qwbxp/image/upload/v1436767990/orchard5_zm1rmh.jpg");
    }
    /*
   private void populateImgString() {
       imgString.add("http://res.cloudinary.com/dklgoqvqb/image/upload/v1440087429/CardStreet/AllCards/rsz_dbs_womans.png");
       imgString.add("http://res.cloudinary.com/dklgoqvqb/image/upload/v1440087428/CardStreet/AllCards/rsz_1posb_everday.png");
   }*/



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            if(parent.equals("bankcardactivity"))  {
                Intent intent = new Intent(CardDetailsActivity.this, BankCardActivity.class);
                intent.putExtra("bankId",bankId);
                CardDetailsActivity.this.startActivity(intent);
            } else if(parent.equals("cardfragment")) {
                Intent intent = new Intent(CardDetailsActivity.this, FIDashBoardActivity.class);
                intent.putExtra("tabNo",1);
                CardDetailsActivity.this.startActivity(intent);
            } else if(parent.equals("promofragment")) {
                Intent intent = new Intent(CardDetailsActivity.this, FIDashBoardActivity.class);
                intent.putExtra("tabNo",2);
                CardDetailsActivity.this.startActivity(intent);
            } else if(parent.equals("walletcardactivity")) {
                Intent intent = new Intent(CardDetailsActivity.this, WalletCardActivity.class);
                CardDetailsActivity.this.startActivity(intent);
            }  else if(parent.equals("interestcardactivity")) {
                Intent intent = new Intent(CardDetailsActivity.this, WalletCardActivity.class);
                CardDetailsActivity.this.startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
