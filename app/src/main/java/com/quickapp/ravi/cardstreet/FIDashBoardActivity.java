package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.quickapp.ravi.adapter.FiFragmentAdapter;
import com.quickapp.ravi.dao.ProfileDao;
import com.quickapp.ravi.model.Profile;


public class FIDashBoardActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        FiFragmentAdapter fiFragmentAdapter = null;
        Intent intent = getIntent();
        int tabNo = intent.getIntExtra("tabNo", 0);

        String drawerState = intent.getStringExtra("drawerState");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fidash_board);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        TextView profileName = (TextView) findViewById(R.id.txtProfileName);
        Profile profile = ProfileDao.getProfile(getApplicationContext());
        if(profile != null) {
            profileName.setText(profile.profileName);
        }

        /* Navigation item*/

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if(menuItem.isChecked())
                    menuItem.setChecked(false);
                else menuItem.setChecked(true);
                Log.e("Inside Menu", "Inside Menu");
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.nav_profile:
                        Intent intent = new Intent(FIDashBoardActivity.this, ProfileActivity.class);
                        FIDashBoardActivity.this.startActivity(intent);
                        return true;
                    case R.id.nav_preference:
                        intent = new Intent(FIDashBoardActivity.this, InterestCardActivity.class);
                        FIDashBoardActivity.this.startActivity(intent);
                        return true;
                    case R.id.nav_wallet:
                        intent = new Intent(FIDashBoardActivity.this, WalletCardActivity.class);
                        FIDashBoardActivity.this.startActivity(intent);
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(),"Default Wrong",Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        /*Drawer layout*/

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            fiFragmentAdapter = setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(tabNo);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(fiFragmentAdapter.getTabView(i));
        }

        if(drawerState != null && drawerState.equals("open")) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }


    }


    private FiFragmentAdapter setupViewPager(ViewPager viewPager) {
        FiFragmentAdapter adapter = new FiFragmentAdapter(getSupportFragmentManager(),FIDashBoardActivity.this);
        adapter.addFragment(BankFragment.newInstance("abc", "def"), "Bank");
        adapter.addFragment(CardFragment.newInstance("def"), "Card");
        adapter.addFragment(PromotionFragment.newInstance("def"), "Promotion");
        viewPager.setAdapter(adapter);
        return adapter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fidash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //noinspection SimplifiableIfStatement
            case R.id.action_settings:
                finish();
                System.exit(0);


            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
