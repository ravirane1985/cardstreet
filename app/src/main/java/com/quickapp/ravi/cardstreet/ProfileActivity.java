package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.quickapp.ravi.dao.ProfileDao;
import com.quickapp.ravi.model.Profile;


public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Profile profile = ProfileDao.getProfile(getApplicationContext());
        if(profile != null) {
            EditText txtProfileName = (EditText) findViewById(R.id.txtProfileName);
            txtProfileName.setText(profile.profileName);
            EditText txtMailId = (EditText) findViewById(R.id.txtMailId);
            txtMailId.setText(profile.mailId);
            EditText txtContact = (EditText) findViewById(R.id.txtContact);
            txtContact.setText(profile.contact);
            EditText txtAge = (EditText) findViewById(R.id.txtAge);
            txtAge.setText(profile.age);
            EditText txtMonthlyIncome = (EditText) findViewById(R.id.txtMonthlyIncome);
            txtMonthlyIncome.setText(profile.monthlyIncome);
        }




        Button btnSaveProfile = (Button) findViewById(R.id.btnSaveProfile);
        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {

                                                  EditText txtProfileName = (EditText) findViewById(R.id.txtProfileName);
                                                  EditText txtMailId = (EditText) findViewById(R.id.txtMailId);
                                                  EditText txtContact = (EditText) findViewById(R.id.txtContact);
                                                  EditText txtAge = (EditText) findViewById(R.id.txtAge);
                                                  EditText txtMonthlyIncome = (EditText) findViewById(R.id.txtMonthlyIncome);

                                                  Profile profile = new Profile();
                                                  profile.profileName =  txtProfileName.getText() == null? null : txtProfileName.getText().toString();
                                                  profile.age =  txtAge.getText() == null? null : txtAge.getText().toString();
                                                  profile.mailId = txtMailId.getText() == null? null : txtMailId.getText().toString();
                                                  profile.contact =  txtContact.getText() == null? null : txtContact.getText().toString();
                                                  profile.monthlyIncome = txtMonthlyIncome.getText() == null? null : txtMonthlyIncome.getText().toString();
                                                  ProfileDao.saveProfile(getApplicationContext(), profile);

                                                  Intent intent = new Intent(ProfileActivity.this, FIDashBoardActivity.class);
                                                  intent.putExtra("tabNo",0);
                                                  ProfileActivity.this.startActivity(intent);
                                              }
                                          }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home) {
            Intent intent = new Intent(ProfileActivity.this, FIDashBoardActivity.class);
            intent.putExtra("tabNo",0);
            ProfileActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
