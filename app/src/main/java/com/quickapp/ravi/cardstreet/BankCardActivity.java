package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.adapter.CardRecyclerListAdapter;
import com.quickapp.ravi.dao.CardDao;


public class BankCardActivity extends AppCompatActivity {

    private RecyclerView bankCardView;
    private CardRecyclerListAdapter bankCardAdapter;
    int bankId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_card);

        Intent intent = getIntent();
        bankId = intent.getIntExtra("bankId",0);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize recycler view
        bankCardView = (RecyclerView) findViewById(R.id.recyclerviewBankCard);
        bankCardView.setLayoutManager(new LinearLayoutManager(this));
        bankCardAdapter = new CardRecyclerListAdapter(BankCardActivity.this, CardDao.getCards(getApplicationContext(), bankId), "bankcardactivity", bankId);
        bankCardView.setAdapter(bankCardAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bank_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home) {
            Log.e("Pressed Home","Pressed Home");
        }

        if (id == android.R.id.home) {
            Intent intent = new Intent(BankCardActivity.this, FIDashBoardActivity.class);
            intent.putExtra("tabNo",0);
            BankCardActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
