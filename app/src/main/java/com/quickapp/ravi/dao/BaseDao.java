package com.quickapp.ravi.dao;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.quickapp.ravi.util.DatabaseHelper;

/**
 * Created by ravirane on 8/16/15.
 */
public class BaseDao {

    static DatabaseHelper databaseHelper;
    public static SQLiteDatabase db;

    public static void open(Context context) throws SQLException {
        databaseHelper = new DatabaseHelper(context);
        db = databaseHelper.getWritableDatabase();
    }

    public static void close() {
        databaseHelper.close();
    }

    public BaseDao(Context context) {
        this.databaseHelper = new DatabaseHelper(context);
    }
}
