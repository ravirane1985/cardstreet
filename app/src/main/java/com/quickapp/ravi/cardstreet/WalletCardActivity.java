package com.quickapp.ravi.cardstreet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.quickapp.ravi.adapter.CardRecyclerListAdapter;
import com.quickapp.ravi.dao.WalletDao;


public class WalletCardActivity extends AppCompatActivity {

    private RecyclerView walletCardView;
    private CardRecyclerListAdapter walletCardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_card);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize recycler view
        walletCardView = (RecyclerView) findViewById(R.id.recyclerviewWalletCard);
        walletCardView.setLayoutManager(new LinearLayoutManager(this));
        walletCardAdapter = new CardRecyclerListAdapter(WalletCardActivity.this, WalletDao.getWaletCards(getApplicationContext()), "walletcardactivity");
        walletCardView.setAdapter(walletCardAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wallet_card, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            Intent intent = new Intent(WalletCardActivity.this, FIDashBoardActivity.class);
            intent.putExtra("tabNo",0);
            WalletCardActivity.this.startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
