package com.quickapp.ravi.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ravirane on 8/8/15.
 */
public class Bank  implements Serializable {

    public String bankName;
    public int bankId;
    public String bankLogo;
    public int cardCount;
    public String bankContactNo;
    public String country;
    public List<Card> cards;


    public Bank() {
    }

    public Bank(String bankName, int bankId, String bankLogo, int cardCount, String bankContactNo, String country) {
        this.bankName = bankName;
        this.bankId = bankId;
        this.bankLogo = bankLogo;
        this.cardCount = cardCount;
        this.bankContactNo = bankContactNo;
        this.country = country;
    }

    public Bank(String bankName, int bankId, String bankLogo, String bankContactNo, String country) {
        this.bankName = bankName;
        this.bankId = bankId;
        this.bankLogo = bankLogo;
        this.bankContactNo = bankContactNo;
        this.country = country;
    }



}
