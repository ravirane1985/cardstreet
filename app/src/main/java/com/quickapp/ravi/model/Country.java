package com.quickapp.ravi.model;

/**
 * Created by ravirane on 8/17/15.
 */
public class Country {

    public String ctryName;
    public String agreementState;

    public Country(String ctryName, String agreementState) {
        this.ctryName = ctryName;
        this.agreementState = agreementState;
    }
}
